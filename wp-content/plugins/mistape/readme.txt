=== Mistape ===
Contributors: decollete
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6UGPDDUY26MCC
Tags: mistake, mistype, spell, spelling error, report error
Requires at least: 3.9.0
Tested up to: 4.3.1
Stable tag: 1.1.1
License: GPLv2 or later

Mistape allows readers to effortlessly notify site staff about found spelling errors.

== Description ==
Strive to keep your content flawless? Mistape will help!

Let your readers notify you about spelling mistakes they may encounter. Make them feel attentive and helpful, and show your care fixing reported errors.

On Ctrl+Enter event, plugin sends selected text along with paragraph and page URL it belongs to an email address selected in admin settings page. You can choose among administrators and editors, or specify another address.

The plugin is very lightweight. The "press Ctrl+Enter..." caption (or your own text) can be configured to be automatically appended to selected post types, or be inserted anywhere using a shortcode. Disabled features don't get loaded so performance impact is minimized to the lowest notch.

Besides text, caption also can be set as image defined by URL.

Mistape is full of hooks enabling you to modify its behavior the way you like.


Help us with the translation to your language, and we will feature you here :)

== Installation ==
1. Look up "Mistape" and and install it from plugins section of your site's admin area. Alternatively, download zip from WordPress.org Plugin Directory and extract its contents to wp-content/plugins directory.
2. Activate and follow the settings link in the notice you will see at the top. Tick desired checkboxes, save, and that's it!

All settings are stored in a single database entry and get wiped on plugin uninstall, so you can be sure your WP installation won't be clogged if you decide you don't want Mistape anymore (and we're sure you won't :) ).

== Screenshots ==
1. Configuration.
2. Help.
3. How it works.
4. Mail notification.

== Changelog ==
= 1.1.1 =
* added an indent in front of Mistape caption to separate it from previous content. This fixed embedded objects not being processed by WordPress if they were in the end of the post content with Mistape caption enabled.

= 1.1.0 =
* added dialog modes: notification, confirmation, confirm and comment (confirmation is default now)
* added option to override email recipient to post author if post ID is determined
* significantly improved determination of selection context
* improved email contents
* now user gets error message if submission fails
* improved specificity of css styles to avoid conflicts with themes
* disabled execution if visitor's browser is IE until selection extraction logic is implemented for it
* various fixes

= 1.0.8 =
* don't output scripts and styles when no caption displayed
* various fixes

= 1.0.7 =
* changed post type check logic to minimize caption's chance to appear in post excerpts

= 1.0.6 =
* skip mobile browsers and Internet Explorer < 11
* fixed enabled post types option behavior
* fixed dialog HTML markup

= 1.0.5 =
* fixed hide logo option saving

= 1.0.4 =
* updated Russian translation

= 1.0.3 =
* custom caption text setting
* ability to specify multiple email recipients
* added an option to display a Mistape logo in caption (enabled by default)
* shortcode fixes
* performance improvements

= 1.0.2 =
* fixed Russian translation.
* email template improvements.

= 1.0.1 =
* internal improvements.

= 1.0.0 =
* Initial release.

== Frequently Asked Questions ==
= I've successfully received a few emails and then Mistape stopped working. Why? =
Mistape implements spam-protection checks. A visitor cannot submit more than five reports in 5-minute time frame (per IP address). All subsequent reports are ignored until timeout.
So if Mistape seems to fail sending emails, and you want to test it once more, use a different internet connection, ask your friend to report something, or just wait a few minutes.

= Mistape doesn't seem to work on old Internet Explorer versions. Is that true? =
Yes, since version 1.0.6 Mistape doesn't render itself if visitor's device is detected as a mobile device or IE < 11.

= Can I customize text and style of the caption? =
Yes, hooks are available for that.

= Can I customize the appearance of confirmation dialog? =
Currently no, as this is a bit more complex feature, and plugin is light and robust.
Though, it may be implemented if there is demand.

= There is no support for my language. How to change the text users see? =
"Press Ctrl+Enter" caption can be customized since version 1.0.3. The rest of strings can be translated using hooks.
For strings filters list see help section in Mistape admin page.